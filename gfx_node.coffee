class GFX_Node

  CENTER_OFFSET = 11

  constructor: (@container, @value = 0, @parent = null, @x = null, @y = null) ->
    @_line = null
    @_node = null
    @_lines = []
    
    if container?
      if @parent? then @connectToNode(@parent)
      @_addToDocument(@container)



  # Static comparison function for comparing two GFX_Node instances.
  @compare: (a, b) ->
    a.value - b.value 
    
  
  glowGood: ->
    @_tempClass('glow-good')
  
  glowBad: ->
    @_tempClass('glow-bad')
    
  glowHit: ->
    @_tempClass('glow-hit')
    
  
  _tempClass: (className)  ->
    addClass(@_node, className)
    setTimeout (=> removeClass(@_node, className)), 300
    
    
  _positionLines: ->
    for line in @_lines
      line.position()


  
  _updatePosition: (x, y) ->
    @x = x + CENTER_OFFSET
    @y = y + CENTER_OFFSET
    @_positionLines()

    
    
  position: (x, y) ->
    @_updatePosition(x, y)
    @_node.style.left = @x - CENTER_OFFSET + 'px'
    @_node.style.top  = @y - CENTER_OFFSET + 'px'

   
    
  @centerOffset: ->
    CENTER_OFFSET
    
    

  # Creates the node's DOM element.  
  _createNode: ->
    d = document.createElement 'div'
    d.className = 'node'
    d.innerText = @value
    return d



  # Connects this node to another node, creating a line.
  connectToNode: (node) ->
    l = new GFX_Line(this, node, @container)
    @_lines.push(l)
    node.connectLine(l)


    
  connectLine: (line) ->
    @_lines.push(line)


    
    
  _delete: ->
    @container.removeChild(@_node)
    for line in @_lines
      line._delete()


      
  # Adds the node to the DOM.
  _addToDocument: (@container) ->
    @_node = @_createNode()
    @_makeDraggable()
    #$(@_node).on('dblclick', => @_delete())
    @container.appendChild @_node


    
  # Makes the node draggable.  
  _makeDraggable: ->
    @_node.unselectable = "on";
    @_node.onselectstart = -> return false
    @_node.style.userSelect = @_node.style.MozUserSelect = "none";

    @_node.onmousedown = (event) =>
      mouseOffsetX = event.x - event.target.offsetLeft
      mouseOffsetY = event.y - event.target.offsetTop
  
      initialX = event.target.offsetLeft
      initialY = event.target.offsetTop
  
      if @_node.setCapture then @_node.setCapture()

      addClass(@_node, 'dragging')
        
      @_updatePosition(initialX, initialY)
        
      document.onmousemove = (event) =>
        event = event || window.event
  
        deltaX = event.clientX - mouseOffsetX - initialX
        deltaY = event.clientY - mouseOffsetY - initialY
  
        loggedDeltaX = MOVEMENT_MODIFIER * Math.sqrt(Math.abs(deltaX))
        loggedDeltaY = MOVEMENT_MODIFIER * Math.sqrt(Math.abs(deltaY))
  
        if (deltaX < 0) then loggedDeltaX = loggedDeltaX * -1
        if (deltaY < 0) then loggedDeltaY = loggedDeltaY * -1
        
        @_node.style.left = initialX + loggedDeltaX + 'px'
        @_node.style.top  = initialY + loggedDeltaY + 'px'

        @_updatePosition(initialX + loggedDeltaX, initialY + loggedDeltaY)
        
  
      document.onmouseup = (event) =>
        document.onmousemove = null
        
        if @_node.releaseCapture then @_node.releaseCapture()
  
        @_node.style.left = initialX + 'px'
        @_node.style.top = initialY + 'px'
  
        setTimeout (=> removeClass(@_node, 'dragging')), 300

        @_updatePosition(initialX, initialY)
    