class GFX_Line

  constructor: (@_startNode, @_endNode, @container) ->
    @_line = @_createLine()
    @_addToDocument(@container)

  

  _createLine: ->
    l = document.createElement 'div'
    l.className = 'line'
    return l


  
  position: ->
    # Position left and right.
    @_line.style.left = @_startNode.x + 'px'
    @_line.style.top  = @_startNode.y + 'px';

    # Calculate width and angle.
    opposite = @_endNode.x - @_startNode.x
    adjacent = @_startNode.y - @_endNode.y

    if adjacent < 0
      angle = Math.atan(opposite/adjacent) + Math.PI / 2;
    else
      angle = Math.atan(opposite/adjacent) - Math.PI / 2;

    @_line.style.width = Math.sqrt(opposite*opposite + adjacent*adjacent) + 'px'
    @_line.style.webkitTransform = 'rotate(' + angle + 'rad)'


    
  _addToDocument: (container) ->
    @_line = @_createLine()
    container.appendChild @_line

  

  _delete: ->
    @container.removeChild(@_line)
    