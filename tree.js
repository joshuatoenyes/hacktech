var Tree = function(comparatorFunction) {
  this.compare = comparatorFunction;
  this.root = null;
  this.numElements = 0;
};

var TreeNode = function(key) {
  this.key = key;
  this.parent = null;
  this.leftChild = null;
  this.rightChild = null;
};

Tree.prototype.size = function() {
  return this.numElements;
}

Tree.prototype.getRoot = function () {
  if (this.size() > 0)
    return this.root.key;
  
  return null;
}

Tree.prototype.insert = function(key) {
  var treeNode = new TreeNode(key);

  if (this.numElements > 0) {
    if (this.onInsertRecurse) this.onInsertRecurse(this.root.key);
    this.recursiveInsert(this.root, treeNode);
    return;
  }

  this.root = treeNode;
  this.numElements++;
  debuglog("insert: root(" + this.root.key.value + "): numElements(" + this.numElements + ")");
};

// The internal argument is used for implementation purposes. It is not
// intended for public use. It's purpose is to ease tree reconstruction upon
// removal of a node. When the argument passed for the internal parameter is
// true, find will return a TreeNode rather than the TreeNode's key member.
Tree.prototype.find = function(key, internal) {
  if (this.numElements == 0) {
    debuglog("find: numElements(" + this.numElements + ") empty");
    return null;
  }

  var treeNode = this.recursiveFind(this.root, key);

  if (treeNode == null) {
    debuglog("find: key.value(" + key.value + ") not found");
    return null;
  }

  if (internal) {
    debuglog("find(internal): key.value(" + treeNode.key.value + ")");
    return treeNode;
  }

  debuglog("find: key.value(" + treeNode.key.value + ")");
  return treeNode.key
};

Tree.prototype.remove = function(key) {
  var treeNode = this.find(key, true);

  if (treeNode == null) 
    return treeNode;

  this.numElements--;
  
  // Removing the root
  if (this.root == treeNode) {
    debuglog("remove: parent(" + treeNode.key.value + ")");
    this.root = treeNode.rightChild;

    if (this.root != null) {
      this.root.parent = null;

      if (treeNode.leftChild != null) {
        // An additional decrement is needed because recursiveInsert() will increment.
        this.numElements--;
        this.recursiveInsert(this.root, treeNode.leftChild);
      }
    }
    else {
      this.root = treeNode.leftChild;

      if (this.root != null) {
        this.root.parent = null;
      }
    }

    return;
  }

  // Removing right child
  if (treeNode.parent.rightChild == treeNode) {
    debuglog("remove: treeNode(" + treeNode.key.value + ")");
    treeNode.parent.rightChild = treeNode.rightChild;

    if (treeNode.rightChild != null)
      treeNode.parent.rightChild.parent = treeNode.parent;
  }
    
  // Removing left child
  else {// (treeNode.parent.leftChild == treeNode)
    debuglog("remove: treeNode(" + treeNode.key.value + ")");
    treeNode.parent.leftChild = treeNode.rightChild;

    if (treeNode.rightChild != null)
      treeNode.parent.leftChild.parent = treeNode.parent;
  }

  if (treeNode.leftChild != null) {
    // An additional decrement is needed because recursiveInsert() will increment.
    this.numElements--;
    this.recursiveInsert(treeNode.parent, treeNode.leftChild);
  }
};

// Returns the sub tree for a key (if found) in the form of an array. The array
// shall be populated by in-order traversal if now addition arguments are
// provided (other than key). However, you can provide as a second argument
// the strings "pre" or "post" for pre- or post- ordering.
Tree.prototype.subTreeToArray = function(key, ordering) {
  var treeNode = this.find(key, true);

  if (treeNode == null)
    return null;

  var subTreeArray = new Array();

  if (!ordering) // Default in-order
    this.buildSubTreeInOrder(treeNode, subTreeArray);
  else if (ordering == "pre")
    this.buildSubTreePreOrder(treeNode, subTreeArray);
  else // (ordering == "post")
    this.buildSubTreePostOrder(treeNode, subTreeArray);

  return subTreeArray;
};

//
// Private helper methods. Not for public consumption.
//

// Used by subTreeArray
Tree.prototype.buildSubTreeInOrder = function(treeNode, subTreeArray) {
  if (treeNode.leftChild != null)
    this.buildSubTreeInOrder(treeNode.leftChild, subTreeArray);

  subTreeArray.push(treeNode.key);

  if (treeNode.rightChild != null)
    this.buildSubTreeInOrder(treeNode.rightChild, subTreeArray);
};

// Used by subTreeArray
Tree.prototype.buildSubTreePreOrder = function(treeNode, subTreeArray) {
  subTreeArray.push(treeNode.key);

  if (treeNode.leftChild != null)
    this.buildSubTreePreOrder(treeNode.leftChild, subTreeArray);

  if (treeNode.rightChild != null)
    this.buildSubTreePreOrder(treeNode.rightChild, subTreeArray);
};

// Used by subTreeArray
Tree.prototype.buildSubTreePostOrder = function(treeNode, subTreeArray) {
  if (treeNode.leftChild != null)
    this.buildSubTreePostOrder(treeNode.leftChild, subTreeArray);

  if (treeNode.rightChild != null)
    this.buildSubTreePostOrder(treeNode.rightChild, subTreeArray);

  subTreeArray.push(treeNode.key);
};

// Recursive find algorithm.
Tree.prototype.recursiveFind = function(treeNode, key) {
  if (treeNode == null)
    return null;

  var result = this.compare(key, treeNode.key);
  debuglog("recursiveFind: compare(" + key.value + ", " + treeNode.key.value + "): result(" + result + ")");

  // Node found
  if (result == 0) {
    debuglog("recursiveFind: tree(" + treeNode.key.value + "), key(" + key.value + ")");
    return treeNode;
  }

  // Traverse left
  if (result < 0 ) {
    return this.recursiveFind(treeNode.leftChild, key);
  }

  return this.recursiveFind(treeNode.rightChild, key);
};

// Recursive insertion algorithm.
Tree.prototype.recursiveInsert = function(treeNode, keyNode) {
  var result = this.compare(keyNode.key, treeNode.key);
  debuglog("recursiveInsert: compare(" + keyNode.key.value + ", " + treeNode.key.value + "): result(" + result + ")");

  if (result == 0) {
    debuglog("recursiveInsert: node(" + treeNode.key.value + "), key(" + keyNode.key.value + "): duplicate");
    return;
  }

  // Insert left
  if (result < 0 ) {
    if (treeNode.leftChild == null) {
      treeNode.leftChild = keyNode;
      treeNode.leftChild.parent = treeNode;
      this.numElements++;

      if (this.onInsertLocationFound) this.onInsertLocationFound(keyNode.key, treeNode.key);
      
      debuglog("recursiveInsert: parent(" + treeNode.key.value + "), leftChild(" + keyNode.key.value + "): numElements(" + this.numElements + ")");
      return;
    }

    if (this.onInsertRecurse) this.onInsertRecurse(treeNode.leftChild.key);
    this.recursiveInsert(treeNode.leftChild, keyNode);
    return;
  }

  // Insert right
  if (treeNode.rightChild == null) {
    treeNode.rightChild = keyNode;
    treeNode.rightChild.parent = treeNode;
    this.numElements++;

    if (this.onInsertLocationFound) this.onInsertLocationFound(keyNode.key, treeNode.key);
    debuglog("recursiveInsert: parent(" + treeNode.key.value + "), rightChild(" + keyNode.key.value + "): numElements(" + this.numElements + ")");
    return;
  }

  if (this.onInsertRecurse) this.onInsertRecurse(treeNode.rightChild.key);
  this.recursiveInsert(treeNode.rightChild, keyNode);
};
