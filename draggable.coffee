# Thanks for the inspiration:  
# http://stackoverflow.com/questions/1685326/responding-to-the-onmousemove-event-outside-of-the-browser-window-in-ie


MOVEMENT_MODIFIER = 5;

makeDraggable = (element) ->

  element.unselectable = "on";
  element.onselectstart = -> return false
  element.style.userSelect = element.style.MozUserSelect = "none";
  
  element.onmousedown = (event) ->
    mouseOffsetX = event.x - event.target.offsetLeft
    mouseOffsetY = event.y - event.target.offsetTop
    
    initialX = event.target.offsetLeft
    initialY = event.target.offsetTop

    if element.setCapture then element.setCapture()
    
    document.onmousemove = (event) ->
      event = event || window.event
      
      deltaX = event.clientX - mouseOffsetX - initialX
      deltaY = event.clientY - mouseOffsetY - initialY
      
      loggedDeltaX = MOVEMENT_MODIFIER * Math.sqrt(Math.abs(deltaX))
      loggedDeltaY = MOVEMENT_MODIFIER * Math.sqrt(Math.abs(deltaY))
      
      if (deltaX < 0) then loggedDeltaX = loggedDeltaX * -1
      if (deltaY < 0) then loggedDeltaY = loggedDeltaY * -1
      
      element.style.left = initialX + loggedDeltaX + 'px'
      element.style.top = initialY + loggedDeltaY + 'px'

    document.onmouseup = (event) ->
      document.onmousemove = null
      
      if element.releaseCapture then element.releaseCapture()

      addClass(element, 'return')
      element.style.left = initialX + 'px'
      element.style.top = initialY + 'px'
      
      setTimeout (-> removeClass(element, 'return')), 300
 