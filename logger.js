var CONSOLE_LOGGING = false;
var ERROR_LOGGING = false;

var debuglog = function() {

  if (!CONSOLE_LOGGING)
    return;

  var str = '';

  for (i = 0; i < arguments.length; ++i) 
    str += arguments[i];
  
  console.log(str);
};

var errorlog = function() {

  if (!ERROR_LOGGING)
    return;

  var str = '';

  for (i = 0; i < arguments.length; ++i) 
    str += arguments[i];
  
  console.error(str);
};
