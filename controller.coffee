
tree = new Tree GFX_Node.compare

context = new webkitAudioContext;
oscillator = context.createOscillator()
gainNode = context.createGain()
oscillator.connect(gainNode)
gainNode.connect(context.destination)
oscillator.frequency.value = 0
oscillator.start(0)


term = (message) ->
  t = document.getElementById('term')
  p = document.createElement('p')
  p.innerHTML = '> ' + message
  #t.innerText += message + '\n'
  t.appendChild(p)


  
window.onload = ->
  term('welcome to the binary tree simulator')
  term('brought to you by `Men in Computing` (MIC)')
  term('&copy;2014, all rights to binary trees reserved')
  
createNodes = () ->
  values = document.getElementById('value').value
  values = values.replace(/\s+/g, ',')
  values = values.replace(/,+/g, ',')
  values = values.split(/,/)

  i = 0

  # Builds a new function to insert the next node.
  buildFuncForNextValue = () -> return ->
    val = values[i++]
    if val && tree.find(new GFX_Node(null, val)) == null then createNode(val)
    if val then setTimeout (buildFuncForNextValue)(), getSpeed()

  # Fire-off the chain by inserting the first node.
  setTimeout (buildFuncForNextValue)(), getSpeed()

  document.getElementById('value').value = ''
  
  
totalOps = 0  
  

createNode = (v) ->
  b = document.getElementsByTagName('body')[0]
  value = v || $('#value').val()

  term('inserting ' + v)
  
  lvl = 0
  parent = null
  
  nodeStack = []
  
  incDisplayedSize()
  
  tree.onInsertLocationFound = (node, parentNode) ->
    node.connectToNode(parentNode)
    lvl++
    parent = parentNode
    term('found insert location')
    totalOps++
    node.glowGood()

    
  tree.onInsertRecurse = (destinationNode) ->
    destinationNode.glowBad()
    nodeStack.push destinationNode
    term('comparing ' + value + ' with ' + destinationNode.value + '...')
    totalOps++
    lvl++


  animateInNode = (node, finalX, finalY) ->
    frames = nodeStack.length
    percen = 100/frames
    keyframes = {}
    
    # iterate through each node in the node stack...
    for n, i in nodeStack
      keyframes[i * percen + '%'] = {'left': n.x + 'px', 'top': n.y + 'px'}
      
    keyframes['100%'] = {'left': finalX + 'px', 'top': finalY + 'px'}
    
    anim = CSSAnimations.create(keyframes)
    node._node.style.webkitAnimationName = anim.name
    node._node.style.webkitAnimationDuration = getSpeed() / 1000 + 's'
    setTimeout (-> 
      node.position(finalX, finalY)
      oscillator.frequency.value = 0), getSpeed()
    
    nodeStack = []
    
    
  n = new GFX_Node(b, value, null, 150, 150)
  tree.insert n
  
  oscillator.frequency.value = value

  # the number of nodes at this level in the tree
  numberOfNodes = Math.pow(2, lvl)
  
  windowWidth = window.innerWidth
  
  xPosition = windowWidth / (numberOfNodes + 1)
  
  # new node goes to the right
  if (parent && GFX_Node.compare(n, parent) > 0)
    xPosition = parent.x + Math.pow(xPosition, 1.05 - lvl*.02) - GFX_Node.centerOffset()
    
  # new nodes goes to the left...
  else if (parent && GFX_Node.compare(n, parent) < 0)
    xPosition = parent.x - Math.pow(xPosition, 1.05 - lvl*.02) - GFX_Node.centerOffset()
  
  if lvl == 0 then yPosition = 100 else yPosition = Math.min(2.7*Math.pow(2, lvl/2.2) * 30, (lvl + 1)*50)
    
  animateInNode n, xPosition, yPosition
  
  lvl = 0
  parent = null

  setDisplayedInsComp Math.round(totalOps / tree.size() * 10) / 10
  

  
preOrderAnimatedTraversal = ->
  term('starting pre-order traversal...')
  nodes = tree.subTreeToArray(tree.getRoot(), 'pre')
  animatedTraversal(nodes)
  
  
postOrderAnimatedTraversal = ->
  term('starting post-order traversal...')
  nodes = tree.subTreeToArray(tree.getRoot(), 'post')
  animatedTraversal(nodes)
  
  
inOrderAnimatedTraversal = ->
  term('starting in-order traversal...')
  nodes = tree.subTreeToArray(tree.getRoot())
  animatedTraversal(nodes)



  
animatedTraversal = (nodes) ->
  i = 0
  
  buildFuncForNextNode = -> return ->
    node = nodes[i++]
    if node
      term('visiting ' + node.value + '...')
      node.glowHit()
      oscillator.frequency.value = node.value
      setTimeout (buildFuncForNextNode)(), getSpeed()
    else
      term('traversal complete')
      oscillator.frequency.value = 0

  setTimeout (buildFuncForNextNode)(), getSpeed()
  
  

# Returns the slider speed.  
getSpeed = () ->
  return 10 * (105 - document.getElementById('speed').value)
  

audio = on

  
toggleAudio = ->
  if audio is on
    audio = off
    gainNode.gain.value = 0
  else 
    audio = on
    gainNode.gain.value = 5

toggleAudio()
  


# Generates a random tree.
generateRandomTree = (numNodes)  ->
  nodes = []
  
  while numNodes -= 1
    nodes.push Math.floor(1000 * Math.random())

  i = 0
  node = nodes[i]
  
  # Builds a new function to insert the next node.
  buildFuncForNextNode = (y) -> return -> 
    node = nodes[++i]
    if tree.find(new GFX_Node(null, y)) == null then createNode(y)
    if node then setTimeout (buildFuncForNextNode)(node), getSpeed()
      
  # Fire-off the chain by inserting the first node.
  setTimeout (buildFuncForNextNode)(node), getSpeed()

  
  
incDisplayedSize = ->
  s = document.getElementById 'size'
  size = parseInt(s.innerText)
  s.innerText = ++size
  
  
  
setDisplayedInsComp = (ops) ->
  s = document.getElementById 'ins-comp'
  s.innerText = ops

  
  
setDisplayedFindComp = (ops) ->
  s = document.getElementById 'find-comp'
  s.innerText = ops
  
  

toggleCreateNodesPanel = () ->
  p = document.getElementById('create-nodes-panel')
  if !p.style.opacity or p.style.opacity == '0'
    p.style.opacity = 1 
  else 
    p.style.opacity = 0
  