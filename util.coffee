addClass = (element, className)  ->
  if not element.className.match(new RegExp(className))?
    element.className += ' ' + className + ' '
    
removeClass = (element, className) ->
  element.className = element.className.replace(className, '');
  element.className = element.className.replace(/(\s+)/, ' ');